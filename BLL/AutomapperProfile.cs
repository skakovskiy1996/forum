﻿using AutoMapper;
using System;
using DAL.Entities;
using BLL.Models;
using DAL.Enum;


namespace BLL
{
    public class AutomapperProfile : Profile
    {


        public AutomapperProfile()
        {
            CreateMap<User, UserModelByAdmin>().ForMember(x => x.FirstName, y => y.MapFrom(m => FirstName(m.Name)))
                .ForMember(x=>x.LastName,y=>y.MapFrom(m=> LastName(m.Name)));
            CreateMap<UserModel, User>().ForMember(m => m.Name, x => x.MapFrom(m => m.FirstName + " " + m.LastName));
            CreateMap<Topic, Topic_model>();
            CreateMap<Topic_model, Topic>();
            CreateMap<MessageModel, Message>();
            CreateMap<Message, MessageModel>();
        }
        private string FirstName(string name)
            {
            string[] array = name.Split(' ');
            return array[0];
        }
        private string LastName(string name)
        {
            string[] array = name.Split(' ');
            return array[1];
        }
    }
}
