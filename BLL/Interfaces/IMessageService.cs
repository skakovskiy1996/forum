﻿
using BLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Entities;

namespace BLL.Interfaces
{
    public interface IMessageService
    {
        int CountMessageByid(int id);
        Task AddNewMessage(MessageModel messageModel);
        IEnumerable<object> GetMassageByTopic(int id);
        Task DeleteByIdAsync(int id);
    }
}
