﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Models;


namespace BLL.Interfaces
{
    public interface ITopicService
    {  


        IEnumerable<Topic_model> GetAllTopic();
        Task AddTopicAsync(Topic_model topic, int id);
        Task DeleteTopicById(int id);
        Task<object> GetTopicByIdAsync(int id);

    }
}
