﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;
using DAL.Entities;


namespace BLL.Interfaces
{
   public interface IUserService
    {
        Task<UserModelByAdmin> GetAllUserById(int id);
        string AuthenticateUser(string email, string password);
         Task RegistrationUser(UserModel userModer);
        void UpdateUser(int id, UserModel userModel);
        IEnumerable<object> AmountMessageCreateUser();
         Task DeleteById(int id);
    }
}
