﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
  public    class MessageModel
    {  
        public int UserId { get; set; }
        public int  TopicId { get; set; }
        public string Text { get; set; }
        public DateTime Open { get; set; }
    }
}
