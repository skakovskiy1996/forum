﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
     public class Topic_model
    {  
        public int Id { get;set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Open { get; set; }


    }
}
