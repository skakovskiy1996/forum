﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
     public class UserModelByAdmin
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
    }
}
