﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BLL.Interfaces;
using DAL.Interfaces;
using BLL.Models;
using DAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class MessageService : IMessageService
    {
        private IMapper mapper;
        private IUnitOfWork unitOfWork;
        public MessageService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }
        public int CountMessageByid(int id)
        {
            int result = 0;
            {
                var messages = unitOfWork.MessageRepository.FindWithDetails().Where(x => x.Topic.Id == id);
                var count = messages.GroupBy(u => u.Topic.Id).Select(g => new { Count = g.Count() });
                foreach (var current in count)
                {
                    result = current.Count;
                }
                return result;
            }

        }
        public async Task AddNewMessage(MessageModel messageModel)
        {
            var user = unitOfWork.UserRepository.FindAll().SingleOrDefault(x => x.Id == messageModel.UserId);
            var topic = unitOfWork.TopicRepository.FindAll().SingleOrDefault(x => x.Id == messageModel.TopicId);
            var newmassage = mapper.Map<MessageModel, Message>(messageModel);
            newmassage.Open = DateTime.Now;
            newmassage.Text = messageModel.Text;
            newmassage.User = user;
            newmassage.Topic = topic;
            await unitOfWork.MessageRepository.AddAsync(newmassage);
            await unitOfWork.SaveAsync();
        }
        public IEnumerable<object> GetMassageByTopic(int id)
        {
            var topic = unitOfWork.MessageRepository.FindWithDetails().Where(x => x.Topic.Id == id).OrderByDescending(x => x.Open);
            return topic.Select(x => new { Id = x.Id, User = x.User.Name, Text = x.Text, Open = x.Open });
        }
        public async Task DeleteByIdAsync(int id)
        {
            await unitOfWork.MessageRepository.DeleteByIdAsync(id);
            await unitOfWork.SaveAsync();

        }
    }
}


