﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BLL.Interfaces;
using DAL.Interfaces;
using BLL.Models;
using DAL.Entities;
using System.Linq;
using System.Threading.Tasks;




namespace BLL.Services
{
    public class TopicService:ITopicService
    {
        private IMapper mapper;
        private IUnitOfWork unitOfWork;

        public TopicService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }
        public IEnumerable<Topic_model> GetAllTopic()
        {
            return mapper.Map<IEnumerable<Topic>, IEnumerable<Topic_model>>(unitOfWork.TopicRepository.FindAll().OrderByDescending(x => x.Open ));
        }
        public async Task AddTopicAsync(Topic_model topic, int id)
        {
            var temp = mapper.Map<Topic_model, Topic>(topic);
            var user = unitOfWork.UserRepository.FindAll().SingleOrDefault(x => x.Id == id);
            temp.User = user;
            temp.Open = DateTime.Now;
            await unitOfWork.TopicRepository.AddAsync(temp);
            await unitOfWork.SaveAsync();
        }
        public async Task<object> GetTopicByIdAsync(int id )
        {
            var topic  = await Task.Run(() => unitOfWork.TopicRepository.FindWithDetails().SingleOrDefault(x=>x.Id== id));
             return new { Name = topic.Name, Description = topic.Description, Open = topic.Open, User = topic.User.Name };
        }
        public async Task DeleteTopicById( int id )
        {
            await unitOfWork.TopicRepository.DeleteByIdAsync(id);
            await unitOfWork.SaveAsync();
        }
    }
}
