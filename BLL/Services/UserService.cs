﻿using System;
using System.Collections.Generic;
using BLL.Validation;
using AutoMapper;
using BLL.Interfaces;
using DAL.Interfaces;
using BLL.Models;
using DAL.Entities;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private IMapper mapper;
        private IUnitOfWork unitOfWork;
        private readonly IOptions<AuthOptions> authOptions;

        public UserService(IMapper mapper, IUnitOfWork unitOfWork, IOptions<AuthOptions> authOptions)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.authOptions = authOptions;
        }
        public async Task <UserModelByAdmin> GetAllUserById(int id )
        {
            return  mapper.Map <User, UserModelByAdmin>(await  unitOfWork.UserRepository.GetByIdAsync(id));
        }
        public string AuthenticateUser(string email, string password)
        {
            var user = unitOfWork.UserRepository.FindAll().SingleOrDefault(x => x.Email == email && x.Password == password);
            if (user == null)
            {
                throw new ForumException("", "");
            }
            var token = GeneraJWT(user);
            return token;

        }
        public async Task RegistrationUser(UserModel userModel)
        {
            if (unitOfWork.UserRepository.FindAll().SingleOrDefault(x => x.Email == userModel.Email) == null)
            {
                var newuser = mapper.Map<UserModel, User>(userModel);
                await unitOfWork.UserRepository.AddAsync(newuser);
                await unitOfWork.SaveAsync();

            }
            else
            {
                throw new ForumException("", "");
            }
        }
        public void UpdateUser(int id, UserModel userModel)
        {   
            if ( unitOfWork.UserRepository.FindAll().SingleOrDefault(x=>x.Email == userModel.Email) == null)
                { 
            var user = unitOfWork.UserRepository.FindAll().SingleOrDefault(x => x.Id == id);
            var updateuser = mapper.Map<UserModel, User>(userModel);
            user.Name = updateuser.Name;
            user.Password = updateuser.Password;
            unitOfWork.UserRepository.Update(user);
        }
            else
            {
                throw new ForumException("", "");
            }
        }
        private string GeneraJWT(User user)
        {
            var authParams = authOptions.Value;

            var securityKey = authParams.GetSymetricSecureKey();
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
            };
            claims.Add(new Claim("role", user.Role.ToString()));
            var token = new JwtSecurityToken(authParams.Issuer, authParams.Audience, claims, expires: DateTime.Now.AddSeconds(authParams.TokenLifetime), signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        public IEnumerable<object> AmountMessageCreateUser()

        {
            var messages = unitOfWork.MessageRepository.FindWithDetails();
            var users = unitOfWork.UserRepository.FindAll().Where(x=>x.Role!=0);
            var amountbyname = from user in users
                        join message in messages on user.Id equals message.User.Id into gj
                        from submessage in gj.DefaultIfEmpty()
                        group gj by  user.Id into grouped
                        select new {Id = grouped.Key, Amount  = grouped.Count() > 1 ? grouped.Count() : 0 };
           
            var amountbyuser = amountbyname.Join(users, p => p.Id, x => x.Id, (p, x) => new { Name = x.Name, Id = x.Id, Amount = p.Amount });
            return amountbyuser;
        }
        public async Task DeleteById(int id)
        {
            await unitOfWork.UserRepository.DeleteByIdAsync(id);
            await unitOfWork.SaveAsync();
        }
    }
}