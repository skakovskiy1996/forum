﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Message : BaseEntity
    {
        public string Text { get; set; }
        public DateTime Open { get; set; }
        public int UserId { get; set; }
        public int TopicId { get; set; }
        public User User { get; set; }
        public Topic Topic { get; set; }
    }
}
