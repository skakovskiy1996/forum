﻿  using System;
using System.Collections.Generic;
using System.Text;
using DAL.Enum;

namespace DAL.Entities
{
     public class User:BaseEntity
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public ICollection<Message> Messages { get; set; }
        public ICollection<Topic> Topics { get; set; }
        public Role Role { get; set; }
    }
}
