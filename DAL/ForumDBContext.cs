﻿using Microsoft.EntityFrameworkCore;
using DAL.Entities;
using DAL.Enum;

namespace DAL
{
    public class ForumDBContext : DbContext

    {
        public ForumDBContext()
        {
        }

        public ForumDBContext(DbContextOptions<ForumDBContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Message> Messages { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(u => u.Role).HasDefaultValue(Role.authorized);
        }
    }

}

