﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IMessageRepository : IRepository<Message>
    {
        IEnumerable<Message> FindWithDetails();
    }
}
