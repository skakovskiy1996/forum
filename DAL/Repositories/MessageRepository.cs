﻿using System.Collections.Generic;
using DAL.Interfaces;
using DAL.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
  public    class MessageRepository:IMessageRepository
    {
       
            private readonly ForumDBContext db;
        public MessageRepository(ForumDBContext context)
            {
                this.db = context;
            }

            public IEnumerable<Message> FindAll()
            {
                return db.Messages;
            }
            public async Task<Message> GetByIdAsync(int id)
            {
                return await Task.Run(() => db.Messages.Find(id));

            }
            public async Task AddAsync(Message entity)
        {
           await Task.Run(() => db.Messages.Add(entity));

            }
            public void Update(Message entity)
            {
           
            db.Messages.Update(entity);


            }
            public void Delete(Message entity)
            {
            db.Messages.Remove(entity);
            }
            public async Task DeleteByIdAsync(int id)
            {
                await Task.Run(() => db.Messages.Remove(db.Messages.Find(id)));
            }
        public IEnumerable<Message> FindWithDetails()
        {
            return db.Messages.Include(u => u.User).Include(u=>u.Topic);
        }
    }
    }

