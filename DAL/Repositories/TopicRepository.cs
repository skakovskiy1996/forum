﻿using System.Collections.Generic;
using DAL.Interfaces;
using DAL.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DAL.Repositories
{
    public class TopicRepository:ITopicRepository
    {
        private readonly ForumDBContext db;
        public TopicRepository(ForumDBContext context)
        {
            this.db = context;
        }

        public IEnumerable<Topic> FindAll()
        {
            return db.Topics;
        }
        public async Task<Topic> GetByIdAsync(int id)
        {

            return await Task.Run(() => db.Topics.Find(id));



        }
        public async Task AddAsync(Topic entity)
        {
            await Task.Run(() =>  db.Topics.Add(entity));

        }
        public void Update(Topic entity)
        {
            db.Topics.Update(entity);


        }
        public void Delete(Topic entity)
        {
            db.Topics.Remove(entity);
        }
        public async Task DeleteByIdAsync(int id)
        {
            await Task.Run(() => db.Topics.Remove(db.Topics.Find(id)));
        }
        public IEnumerable<Topic> FindWithDetails ()
        {
            return db.Topics.Include(u => u.User);
        }
    }
}
