﻿using System.Collections.Generic;
using DAL.Interfaces;
using DAL.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DAL.Repositories
{
   public class UserRepository: IUserRepository
    {
        private readonly ForumDBContext db;
        public UserRepository(ForumDBContext context)
        {
            this.db = context;
        }

        public IEnumerable<User> FindAll()
        {  
            return db.Users;
        }
        public async Task<User> GetByIdAsync(int id)
        {
            return await Task.Run(() => db.Users.Find(id));

        }
        public async Task AddAsync(User entity)
        {
            await Task.Run(() => db.Users.Add(entity));
          

        }
        public void Update(User entity)
        {
            db.Users.Update(entity);
            db.SaveChanges();


        }
        public void Delete(User entity)
        {
            db.Users.Remove(entity);
        }
        public async Task DeleteByIdAsync(int id)
        {
            await Task.Run(() => db.Users.Remove(db.Users.Find(id)));
        }
      
      
    }
}
