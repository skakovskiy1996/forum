﻿using System;
using DAL.Interfaces;
using DAL.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {

        public ForumDBContext context ;

        public UnitOfWork(ForumDBContext context)
        {
            this.context = context;
        }
       
        public IUserRepository UserRepository
        {
            get => new UserRepository(context);

        }

        public ITopicRepository TopicRepository
        {
            get => new TopicRepository(context);
        }
        public IMessageRepository MessageRepository
        {
            get => new MessageRepository(context);
        }
        public async Task<int> SaveAsync()
        {
            return await context.SaveChangesAsync();
        }
    }
}
