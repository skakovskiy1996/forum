﻿using DAL.Entities;
using NUnit.Framework;
using DAL;
using DAL.Repositories;
using System.Linq;
using System.Threading.Tasks;
using DAL.Enum;
using System;

namespace my_app.Tests
{
    [TestFixture]
    class MessageRepositoryTests
    {
        [Test]
        public void MessageRepositoryFindAllReturnsAllValues()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var messageRepository = new MessageRepository(context);
                var topics = messageRepository.FindAll().ToList();
                Assert.AreEqual(2, topics.Count());
            }
        }
        [Test]
        public async Task MessageRepositoryGetByIdReturnsSingleValue()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var messageRepository = new MessageRepository(context);
                var message = await messageRepository.GetByIdAsync(1);

                Assert.AreEqual(1, message.Id);
                Assert.AreEqual("Nice", message.Text);
                Assert.AreEqual(new DateTime(2020, 8, 23), message.Open);
            }

        }
        [Test]
        public async Task MessageRepositoryAddAsyncAddsValueToDatabase()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var messageRepository = new MessageRepository(context);
                var message = new Message() { Id = 3 };

                await messageRepository.AddAsync(message);
                await context.SaveChangesAsync();

                Assert.AreEqual(3, context.Messages.Count());
            }
        }

        [Test]
        public async Task MessageRepositoryDeleteByIdAsyncDeletesEntity()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var messageRepository = new MessageRepository(context);

                await messageRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();

                Assert.AreEqual(1, context.Messages.Count());
            }
        }

        [Test]
        public void MessageRepository_Update_UpdatesEntity()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var messageRepository = new MessageRepository(context);

                var newmessage = new Message() { Id = 1, Text = "Good" , Open = new DateTime(2021, 8, 23) };

                messageRepository.Update(newmessage);
                var message = messageRepository.FindAll().SingleOrDefault(x => x.Id == newmessage.Id );


                Assert.AreEqual(1, message.Id);
                Assert.AreEqual("Good", message.Text);
                Assert.AreEqual(new DateTime(2021, 8, 23), message.Open);
            }
        }
        [Test]

        public void MessageRepositoryGetAllWithDetailsReturnAllValues()
        {

            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                //arrange
                var messageRepository = new MessageRepository(context);

                //act
                var messages = messageRepository.FindWithDetails().ToList();


                //assert
                Assert.AreEqual(2, messages.Count);
                Assert.IsNotNull(messages[0].User);
                Assert.IsNotNull(messages[0].Topic);
            }

        }

    }
}
