﻿using DAL.Entities;
using NUnit.Framework;
using DAL;
using DAL.Repositories;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace my_app.Tests
{
    [TestFixture]
    class TopicRepositoryTests
    {
        [Test]
        public void TopicRepositoryFindAllReturnsAllValues()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var topicRepository = new TopicRepository(context);
                var topics = topicRepository.FindAll().ToList();
                Assert.AreEqual(2, topics.Count());
            }
        }
        [Test]
        public async Task TopicRepositoryGetByIdReturnsSingleValue()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var topicRepository = new TopicRepository(context);
                var topic = await topicRepository.GetByIdAsync(1);

                Assert.AreEqual(1, topic.Id);
                Assert.AreEqual("Computer problem", topic.Name);
                Assert.AreEqual("Computer doesn't turn on", topic.Description);
                Assert.AreEqual(new DateTime(2020, 7, 23), topic.Open);
            }

        }
        [Test]
        public async Task TopicRepositoryAddAsyncAddsValueToDatabase()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var topicRepository = new TopicRepository(context);
                var topic = new Topic() { Id = 3 };

                await topicRepository.AddAsync(topic);
                await context.SaveChangesAsync();

                Assert.AreEqual(3, context.Topics.Count());
            }
        }

        [Test]
        public async Task TopicRepositoryDeleteByIdAsyncDeletesEntity()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var topicRepository = new TopicRepository(context);

                await topicRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();

                Assert.AreEqual(1, context.Topics.Count());
            }
        }

        [Test]
        public void TopicRepository_Update_UpdatesEntity()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var topicRepository = new TopicRepository(context);

                var topic = new Topic() { Id = 1, Name = "What about news", Description = "Do you have interesting news about computer", Open = new DateTime(2021, 7, 23) };

                topicRepository.Update(topic);
                var newtopic = topicRepository.FindAll().SingleOrDefault(x => x.Id == topic.Id);


                Assert.AreEqual(1, newtopic.Id);
                Assert.AreEqual("What about news", newtopic.Name);
                Assert.AreEqual("Do you have interesting news about computer", newtopic.Description);
                Assert.AreEqual(new DateTime(2021, 7, 23), newtopic.Open);
            }
        }
        [Test]

        public void TopicRepositoryGetAllWithDetailsReturnAllValues()
        {

            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                //arrange
                var topicRepository = new TopicRepository(context);

                //act
                var topics = topicRepository.FindWithDetails().ToList();


                //assert
                Assert.AreEqual(2, topics.Count);
                Assert.IsNotNull(topics[0].User);
            }

        }

    }
}
