﻿using DAL.Entities;
using NUnit.Framework;
using DAL;
using DAL.Repositories;
using System.Linq;
using System.Threading.Tasks;
using DAL.Enum;

namespace my_app.Tests
{
    [TestFixture]
    class UserRepositoryTests
    {
        [Test]
        public void UserRepositoryFindAllReturnsAllValues()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UserRepository(context);
                var users = usersRepository.FindAll().ToList();
                Assert.AreEqual(2, users.Count());
            }
        }
        [Test]
        public async Task UserRepositoryGetByIdReturnsSingleValue()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UserRepository(context);
                var user = await usersRepository.GetByIdAsync(1);

                Assert.AreEqual(1, user.Id);
                Assert.AreEqual("condor@gmail.com", user.Email);
                Assert.AreEqual("Dima", user.Name);
                Assert.AreEqual("1234F", user.Password);
                Assert.AreEqual(Role.authorized, user.Role);
            }
        }
        [Test]
        public async Task UserRepositoryAddAsyncAddsValueToDatabase()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UserRepository(context);
                var user = new User() { Id = 3 };

                await usersRepository.AddAsync(user);
                await context.SaveChangesAsync();

                Assert.AreEqual(3, context.Users.Count());
            }
        }

        [Test]
        public async Task UserRepositoryDeleteByIdAsyncDeletesEntity()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UserRepository(context);

                await usersRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();

                Assert.AreEqual(1, context.Users.Count());
            }
        }

        [Test]
        public void UserRepository_Update_UpdatesEntity()
        {
            using (var context = new ForumDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UserRepository(context);

                var user = new User() { Id = 1, Name = "John Travolta", Email = "join@gmail.com", Age = 56 };

                usersRepository.Update(user);


                Assert.AreEqual(1, user.Id);
                Assert.AreEqual("John Travolta", user.Name);
                Assert.AreEqual("join@gmail.com", user.Email);
                Assert.AreEqual(56, user.Age);
            }
        }

    }
}