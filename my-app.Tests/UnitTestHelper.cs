﻿using DAL.Entities;
using DAL;
using Microsoft.EntityFrameworkCore;
using System;
using DAL.Enum;

namespace my_app.Tests
{
    internal static class UnitTestHelper
    {


        public static DbContextOptions<ForumDBContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<ForumDBContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new ForumDBContext(options))
            {
                SeedData(context);
            }
            return options;
        }
        public static void SeedData(ForumDBContext context)
        {
            context.Add(new User { Id = 1, Age = 18, Email = "condor@gmail.com", Name = "Dima", Password = "1234F", Role = Role.authorized });
            context.Add(new User { Id = 2, Age = 25, Email = "Shevchur1992@gmail.com", Name = "Vlad", Password = "Vlad121", Role = Role.authorized });
            context.Add(new Topic { Id = 1, UserId = 1, Name = "Computer problem", Description = "Computer doesn't turn on", Open = new DateTime(2020, 7, 23) });
            context.Add(new Topic { Id = 2, UserId = 1, Name = "Can you help with laptop", Description = "I can't choose laptop", Open = new DateTime(2020, 7, 23) });
            context.Add(new Message { Id = 1, TopicId = 1, UserId = 1, Text = "Nice", Open = new DateTime(2020, 8, 23) });
            context.Add(new Message { Id = 2, TopicId = 2, UserId = 1, Text = "I don't know", Open = new DateTime(2020, 8, 23) });
            context.SaveChanges();
        }

    }
}
