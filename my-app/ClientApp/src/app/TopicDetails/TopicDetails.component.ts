import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
    selector: 'app-topicdetail',
    templateUrl: './TopicDetails.component.html',
    styleUrls: ['./TopicDetails.component.css']
})

export class TopicDetailComponent { 
    public topicWithDetails:TopicWithDetails = <any>{};
    public amount:number;
    public massage:Massages[];
    public text:string;


    constructor(private route: ActivatedRoute , private http: HttpClient, private jWhelper:JwtHelperService) { }
    ngOnInit() {
        const routeParams = this.route.snapshot.paramMap;
        const productIdFromRoute = Number(routeParams.get('topicId'));
        this.http
        .get<TopicWithDetails>(`https://localhost:44315/api/Topic/${productIdFromRoute}`).subscribe( result => {
            this.topicWithDetails = result;
        });
        this.http
        .get<number>(`https://localhost:44315/api/Massage/${productIdFromRoute}`).subscribe( result => {
            this.amount = result;
        });
        this.http
        .get<Massages[]>(`https://localhost:44315/api/Massage/amount/${productIdFromRoute}`)
        .subscribe(
            result => {
                this. massage = result;
            },
           
        );
    }
    addMessage(text:string) {
        const routeParams = this.route.snapshot.paramMap;
        const productIdFromRoute = Number(routeParams.get('topicId'));
        var TopicId = productIdFromRoute;
        const body = {text,TopicId};
        this.http.post("https://localhost:44315/api/Massage", body).subscribe((result:any) => {

            this.ngOnInit();
            
        })
    }
    deleteMassage(id:number)
    {
        
        this.http.delete(`https://localhost:44315/api/Massage/?id=${id}`).subscribe((result:any) => {

            this.ngOnInit();
            
        });
    }
    public get IsAdminIn():boolean {
        const token: string = localStorage.getItem("jwt");
        const decodedToken = this.jWhelper.decodeToken(token);
        if(!!token && decodedToken.role == "admin" && !this.jWhelper.isTokenExpired(token))
        {
            return true;
        }
        return false;
    }
    public get isLoggedIn(): boolean {
        const token: string = localStorage.getItem("jwt");
        if (!!token && !this.jWhelper.isTokenExpired(token)) {
            return true;
        }
        return false;
    }
}
interface TopicWithDetails {
    name: string;
    open: Date;
    description: string;
    user:string;
}
interface Massages{
    id:number;
    user: string;
    text:string;
    open:Date;
}