import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from 'src/environments/environment';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import {  HttpClientModule } from '@angular/common/http';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegComponent } from './registration/registration.component';
import { AuthGuard } from './guards/authguard.serve';
import { AdminGuard } from './guards/adminguard.serve';
import { ChangeUserComponent } from './changeuser/changeuser.component'; 
import { TopicComponent } from './topic/topic.component';
import { TopicDetailComponent } from './TopicDetails/TopicDetails.component';
import { UsersComponent } from './users/users.component';
export function tokenGetter(){
  return localStorage.getItem("jwt"); 
}
@NgModule({
  declarations: [
    NavMenuComponent,
    AppComponent,
    LoginComponent,
    RegComponent,
    ChangeUserComponent,
    TopicComponent,
    TopicDetailComponent,
    UsersComponent 

  ],
  imports: [
    FormsModule,
    BrowserModule,
    RouterModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component:  TopicComponent},
      { path: 'topic/:topicId', component:   TopicDetailComponent  },
      { path: 'reg', component: RegComponent },
      { path: 'loign', component: LoginComponent  },
      { path: 'changeuser', component: ChangeUserComponent, canActivate: [AuthGuard] },
      { path: 'alluser', component: UsersComponent, canActivate: [AdminGuard] },
    ]),
    JwtModule.forRoot({
    config: {
      tokenGetter: tokenGetter,
      allowedDomains:["localhost:44315"],
      disallowedRoutes:[]
    }

    })
   

    
  ],
  providers: [ AuthGuard , AdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }