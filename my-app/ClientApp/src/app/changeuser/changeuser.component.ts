import { HttpClient } from '@angular/common/http'
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "../models/user";

@Component({
    selector: 'app-changeuser',
    templateUrl: './changeuser.component.html',
    styleUrls: ['./changeuser.component.css']
    
    
})
export class ChangeUserComponent { 
    invalidLogin:boolean;
    user: User = new User() // данные вводимого пользователя
    
    constructor(private routet: Router,  private http: HttpClient) {}
    submit(user: User) {
      

        this.http.put("https://localhost:44315/api/Auth/change", user).subscribe(data => {


            this.routet.navigate(['']);
        },
            error => { 
                
               this. invalidLogin = true;
            });
}
  }