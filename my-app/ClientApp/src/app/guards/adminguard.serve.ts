import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
import { CanActivate,Router } from "@angular/router";


@Injectable()
export class AdminGuard implements CanActivate {
    constructor (private router :Router , private jWhelper: JwtHelperService) {}
    
    canActivate() {
        const token: string = localStorage.getItem("jwt");
        const decodedToken = this.jWhelper.decodeToken(token);
        if(decodedToken.role == "admin" && !this.jWhelper.isTokenExpired(token )){
         return true;
     }
     this.router.navigate(['']);
     return false;
    }
    
}