import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { templateJitUrl } from "@angular/compiler";
import { Observable, tap } from 'rxjs';
import { User } from "../models/user";


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    invalidLogin: boolean;
    user: User = new User() 
    constructor(private routet: Router, private http: HttpClient) { }

    login(user: User) {


        this.http.post("https://localhost:44315/api/Auth/login", user).subscribe(data => {


            const token = (<any>data).token;
            localStorage.setItem("jwt", token);
            this.invalidLogin = false;
            this.routet.navigate([""]);
        },
            error => {
                this.invalidLogin = true;
            });

    }
}