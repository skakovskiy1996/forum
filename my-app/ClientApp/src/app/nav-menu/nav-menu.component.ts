import { Component, OnInit, DoCheck } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';


@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit,DoCheck{
  constructor (  private jWhelper: JwtHelperService) {
  }
  ngDoCheck(): void {
    this.ngOnInit();
  }
  ngOnInit(){
    const token: string = localStorage.getItem("jwt");
    this. decodedToken = this.jWhelper.decodeToken(token);
  }
   decodedToken:any;
  public get isLoggedIn():boolean{
     const token: string = localStorage.getItem("jwt");
     if(!!token && !this.jWhelper.isTokenExpired(token))
     {
       return true;
     }
     return false;
  }
  public get isLoggedAdminIn():boolean{
    const token: string = localStorage.getItem("jwt");
    const decodedToken = this.jWhelper.decodeToken(token);
    if(decodedToken.role == "admin" && !this.jWhelper.isTokenExpired(token )){
     return true;
 }
 return false;
}

   Logout(){
     localStorage.removeItem("jwt");
   }
}
