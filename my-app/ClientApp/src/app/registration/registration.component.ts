import { HttpClient } from '@angular/common/http'
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "../models/user";

@Component({
    selector: 'app-reg',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
    
})
export class RegComponent { 
    invalidLogin:boolean;
    user: User = new User() // данные вводимого пользователя
    
    constructor(private routet: Router,  private http: HttpClient) {}
    submit(user: User) {
      

        this.http.post("https://localhost:44315/api/Auth/reg", user).subscribe(data => {


            this.routet.navigate(['']);
        },
            error => { 
                
               this. invalidLogin = true;
            });
}
  }