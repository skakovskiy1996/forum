import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Newtopic } from '../models/topic';
import { Router } from "@angular/router";

@Component({
    selector: 'app-topic',
    templateUrl: './topic.companent.html'
})
export class TopicComponent implements  OnInit  {
    public topic: Topic[];
    newtopic: Newtopic = new Newtopic();
    isInputTopic: boolean = false;

    constructor(private http: HttpClient, private jWhelper: JwtHelperService, private routet: Router) { }
    ngOnInit() {
        this.http
            .get<Topic[]>("https://localhost:44315/api/Topic")
            .subscribe(
                result => {
                    this.topic = result;
                },
               
            );
            
            
            
    }
    public get IsAdminIn():boolean {
        const token: string = localStorage.getItem("jwt");
        const decodedToken = this.jWhelper.decodeToken(token);
        if(!!token && decodedToken.role == "admin" && !this.jWhelper.isTokenExpired(token))
        {
            return true;
        }
        return false;
    }
    

    public get isLoggedIn(): boolean {
        const token: string = localStorage.getItem("jwt");
        if (!!token && !this.jWhelper.isTokenExpired(token)) {
            return true;
        }
        return false;
    }
    addTopic(newtopic: Newtopic) {
        this.http.post("https://localhost:44315/api/Topic", newtopic).subscribe((result:Topic) => {
            this.isInputTopic = false;
        this.ngOnInit();        
    })
    }
    DeleteTopic(id:Number)
    {
        this.http.delete(`https://localhost:44315/api/Topic/${id}`).subscribe((result:any) => {

            this.ngOnInit();
            
        });
        
    }
    createTopic() {
        this.isInputTopic = true;
    }
    Cancel(){
        this.isInputTopic = false;
    }
} 

interface Topic {
    name: string;
    open: Date;
}
