import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Newtopic } from '../models/topic';
import { Router } from "@angular/router";


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  moredetail:boolean;
  users:Users[];
  userdetail:UserDetail;
  isDetail:boolean=false;
  constructor(private http: HttpClient, private jWhelper: JwtHelperService, private routet: Router) { }

  ngOnInit() {
    this.http
            .get<Users[]>("https://localhost:44315/api/Auth/AmountMessage")
            .subscribe(
                result => {
                    this.users = result;
                },           
            );
  } 
  Detail(id:string){
   this.http.get<UserDetail>(`https://localhost:44315/api/Auth?id=${id}`).subscribe(
    result => {
        this.userdetail = result;
        this.isDetail = true;
    },           
);
  }
  Return(){
    this.isDetail =false;
  }
  Delete(id:string){
    this.http.delete(`https://localhost:44315/api/Auth?id=${id}`).subscribe(
     result => {
         this.ngOnInit();
     },           
 );
}
}
interface Users {
  name:string;
  amount:number;
}
interface UserDetail
{
  firstName:string;
  lastName:string;
  email:string;
  age:number;
}