﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;


namespace my_app.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class AuthController : ControllerBase
    {
        public readonly IUserService userService;
 

        public AuthController(IUserService userService)
        {
            this.userService = userService;
        }

        [Route("login")]
        [HttpPost]
        public IActionResult Login([FromBody] Login request)
        {
            try
            {
                var token = userService.AuthenticateUser(request.Email, request.Password);
                return Ok(new { Token = token });
            }
            catch (ForumException)
            { 
            return Unauthorized();
            }
        }
        [Route("reg")]
        [HttpPost]
        public async Task<ActionResult>  Registration([FromBody] UserModel user)
        {
            try
            {
                await userService.RegistrationUser(user);
                return Ok();
            }
            catch(ForumException)
            {
                return BadRequest();
            }
        }
        [Route("change")]
        [HttpPut]
        [Authorize(Roles = "authorized")]
        public ActionResult ChangeProfileUser([FromBody] UserModel user)
        {   
            var UserId = User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;
            userService.UpdateUser(int.Parse(UserId), user);
            return Ok();
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<User>>> GetUserById(int id)
        {
            return Ok( await userService.GetAllUserById(id));
        }
        [HttpGet]
        [Route("AmountMessage")]
        [Authorize(Roles = "admin")]
        public ActionResult<object> GetUserwithAmount()
        {
            return Ok(userService.AmountMessageCreateUser());
        }
        [HttpDelete] 
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> DeleteUserByid(int id)
        {
            await userService.DeleteById(id);
            return Ok(true);
        }
    }
}