﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DAL.Interfaces;
using DAL.Entities;
using BLL.Models;
using BLL.Services;

namespace my_app.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        public readonly IUnitOfWork unitOfWork;
        


        public HomeController(IUnitOfWork unitOfWork)
        {
         
            this.unitOfWork = unitOfWork;
        }
 
        public ActionResult<IEnumerable<User>> GetAll()
        {
            return Ok(unitOfWork.UserRepository.FindAll().ToList());
        }
        [HttpGet()]
        public Login Get()
        {
            return new Login() { Email = "fdd" };
                
        }
    }
}

