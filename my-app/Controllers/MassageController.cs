﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using BLL.Interfaces;
using BLL.Models;
using System.Collections.Generic;

namespace my_app.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class MassageController : ControllerBase
    {
        private readonly IMessageService messageService;

        public MassageController(IMessageService messageService)
        {
            this.messageService = messageService;

        }

        [HttpGet]
        [Route("amount/{id}")]
        public ActionResult<IEnumerable<object>> GetMessageByTopic(int id)
        {
            return Ok(messageService.GetMassageByTopic(id));
        }
        [HttpGet]
        [Route("{id}")]
        public ActionResult GetAmountMessage(int id)
        {
            return Ok(messageService.CountMessageByid(id));
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddMassage( MessageModel message)
        {
            var UserId = User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;
            message.UserId = int.Parse(UserId);
            await messageService.AddNewMessage(message);
            return Ok(true);

        }
        [HttpDelete]
        [Authorize]
        public async Task<ActionResult> Delete(int id)
        {
            await messageService.DeleteByIdAsync(id);
            return Ok(true);
        }
    }
}



