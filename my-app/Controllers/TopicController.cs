﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using BLL.Interfaces;
using BLL.Models;



namespace my_app.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TopicController : ControllerBase
    {
        public readonly ITopicService topicrService;

        public TopicController(ITopicService topicrService)
        {
            this.topicrService = topicrService;

        }
        [HttpGet]

        [Route("")]
        public ActionResult GetAll()
        {
            return Ok(topicrService.GetAllTopic());
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<Task<object>>> GetTopicById(int id)
        {

            var result = await topicrService.GetTopicByIdAsync(id);
            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Add(Topic_model topic)
        {
            var UserId = User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;
            await topicrService.AddTopicAsync(topic, int.Parse(UserId));
            return Ok(topic);
        }
        [HttpDelete]
        [Authorize(Roles = "admin")]
        [Route("{id}")]

        public async Task<IActionResult> DeleteTopicbyId(int id)
        {
            await topicrService.DeleteTopicById(id);
            return Ok(true);
        }
    }
}
