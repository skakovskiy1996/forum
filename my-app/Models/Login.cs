﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
  public  class Login 
    {
       [Required(ErrorMessage = "Идентификатор пользователя не установлен")]
       public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
